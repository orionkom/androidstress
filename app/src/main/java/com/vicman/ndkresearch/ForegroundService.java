package com.vicman.ndkresearch;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ForegroundService extends Service {
    private static final String TAG = "ForegroundService";
    public interface ACTION {


        public static String MAIN = "action.main";
        public static String SEND = "action.send";
        public static String STARTFOREGROUND = "action.startforeground";
        public static String STOPFOREGROUND = "action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }


    final ExecutorService exService = Executors.newSingleThreadExecutor();
    private StressRunnable stressRunnable = new StressRunnable();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final String action;
        if (intent != null && (action = intent.getAction()) != null) {
            //Log.i(TAG, "Received Start Foreground Intent: " + intent.getAction());
            if (action.equals(ACTION.STARTFOREGROUND)) {
                Log.i(TAG, "Received Start Foreground Intent ");
                Intent notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.setAction(ACTION.MAIN);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                        notificationIntent, 0);

                Intent hackIntent = new Intent(this, ForegroundService.class);
                hackIntent.setAction(ACTION.SEND);
                PendingIntent pHackIntent = PendingIntent.getService(this, 0,
                        hackIntent, 0);

                Intent stopCloseIntent = new Intent(this, ForegroundService.class);
                stopCloseIntent.setAction(ACTION.STOPFOREGROUND);
                PendingIntent pStopCloseIntent = PendingIntent.getService(this, 0,
                        stopCloseIntent, 0);

                Notification notification = new NotificationCompat.Builder(this)
                        .setContentTitle("Стресс нагрузка")
                        .setTicker("Стресс - запущен")
                        .setContentText("Запущен")
                        .setSmallIcon(android.R.drawable.stat_sys_warning)
                        .setContentIntent(pendingIntent)
                        .setOngoing(true)
                        .addAction(android.R.drawable.ic_menu_call,
                                "+ 1 task", pHackIntent)
                        .addAction(android.R.drawable.ic_menu_close_clear_cancel,
                                "Остановить", pStopCloseIntent)
                        .build();
                startForeground(NOTIFICATION_ID.FOREGROUND_SERVICE,
                        notification);
                startStress();

            } else if (action.equals(
                    ACTION.SEND)) {
                Log.i(TAG, "Received Start Foreground Intent");
                startStress();
            } else if (action.equals(
                    ACTION.STOPFOREGROUND)) {

                exService.shutdown();
                Log.i(TAG, "Received Stop Foreground Intent");
                stopForeground(true);
                stopSelf();
            }
        }

        return START_STICKY;
    }

    private void startStress() {
        if (exService != null && !exService.isTerminated()) {
            exService.submit(stressRunnable);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case of bound services.
        return null;
    }


    private class StressRunnable implements Runnable {
        @Override
        public void run() {
            try {
                final int cpu = PrefActivity.getCPU(getApplicationContext());
                final int io = PrefActivity.getIO(getApplicationContext());
                final int hdd = PrefActivity.getHDD(getApplicationContext());
                final int timeout = PrefActivity.getDuration(getApplicationContext());

                StringBuilder sb = new StringBuilder("--verbose --vm 2 --vm-bytes 128M");
                sb.append(" --timeout " + (timeout + 1)*10 + "s");
                if (io > 0)  sb.append(" --io " + io);
                if (cpu > 0)  sb.append(" --cpu " + cpu);
                if (hdd > 0)  sb.append(" --hdd " + hdd);

                stringFromJNI(sb.toString());
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    public native String stringFromJNI(String timeout);


    /* this is used to load the 'hello-jni' library on application
     * startup. The library has already been unpacked into
     * /data/data/com.example.hellojni/lib/libhello-jni.so at
     * installation time by the package manager.
     */
    static {
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("hello-jni");
    }
}