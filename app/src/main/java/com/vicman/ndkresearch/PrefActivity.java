package com.vicman.ndkresearch;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class PrefActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
    }

    private static SharedPreferences getSharedPreferences(@NonNull Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static int getDuration(@NonNull Context context) {
        return getSharedPreferences(context).getInt("Duration", 2);
    }
    public static void setDuration(@NonNull Context context, int value) {
        getSharedPreferences(context).edit().putInt("Duration", value).apply();
    }

    public static int getCPU(@NonNull Context context) {
        return getSharedPreferences(context).getInt("CPU", 2);
    }
    public static void setCPU(@NonNull Context context, int value) {
        getSharedPreferences(context).edit().putInt("CPU", value).apply();
    }

    public static int getIO(@NonNull Context context) {
        return getSharedPreferences(context).getInt("IO", 4);
    }
    public static void setIO(@NonNull Context context, int value) {
        getSharedPreferences(context).edit().putInt("IO", value).apply();
    }

    public static int getHDD(@NonNull Context context) {
        return getSharedPreferences(context).getInt("HDD", 0);
    }
    public static void setHDD(@NonNull Context context, int value) {
        getSharedPreferences(context).edit().putInt("HDD", value).apply();
    }



}