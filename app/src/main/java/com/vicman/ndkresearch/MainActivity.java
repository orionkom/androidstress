package com.vicman.ndkresearch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, ForegroundService.class);
                startIntent.setAction(ForegroundService.ACTION.STARTFOREGROUND);
                startService(startIntent);
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, ForegroundService.class);
                startIntent.setAction(ForegroundService.ACTION.STOPFOREGROUND);
                startService(startIntent);
            }
        });


        final SeekBar seekCPU = (SeekBar) findViewById(R.id.seekCPU);
        final SeekBar seekIO = (SeekBar) findViewById(R.id.seekIO);
        final SeekBar seekHDD = (SeekBar) findViewById(R.id.seekHDD);
        final SeekBar seekDuration = (SeekBar) findViewById(R.id.seekDuration);

        final TextView tCPU = (TextView) findViewById(R.id.tCPU);
        final TextView tIO = (TextView) findViewById(R.id.tIO);
        final TextView tHDD = (TextView) findViewById(R.id.tHDD);
        final TextView tDuration = (TextView) findViewById(R.id.tDuration);


        SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar == seekCPU) {
                    tCPU.setText(String.format("CPU (spawn %d workers spinning on sqrt())", progress));
                    PrefActivity.setCPU(getApplicationContext(), progress);
                } else if (seekBar == seekIO) {
                    tIO.setText(String.format("IO (spawn %d workers spinning on sync())", progress));
                    PrefActivity.setIO(getApplicationContext(), progress);
                } else if (seekBar == seekHDD) {
                    tHDD.setText(String.format("HDD (spawn %d workers spinning on write()/unlink())", progress));
                    PrefActivity.setHDD(getApplicationContext(), progress);
                } else if (seekBar == seekDuration) {
                    tDuration.setText("Task duration: " + (progress+1)*10 + "sec");
                    PrefActivity.setDuration(getApplicationContext(), progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
        seekCPU.setOnSeekBarChangeListener(listener);
        seekIO.setOnSeekBarChangeListener(listener);
        seekHDD.setOnSeekBarChangeListener(listener);
        seekDuration.setOnSeekBarChangeListener(listener);

        seekCPU.setProgress(PrefActivity.getCPU(this));
        seekIO.setProgress(PrefActivity.getIO(this));
        seekHDD.setProgress(PrefActivity.getHDD(this));
        seekDuration.setProgress(PrefActivity.getDuration(this));
    }



}
