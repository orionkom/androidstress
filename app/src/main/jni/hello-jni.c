/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <stdio.h>
#include <string.h>
#include <jni.h>
#ifndef ADD_H
#define ADD_H
#include <stress.h>
#endif /* ADD_H - Google "include guard" for more info about this trickery */

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
jstring
Java_com_vicman_ndkresearch_ForegroundService_stringFromJNI( JNIEnv* env,
                                                  jobject thiz, jstring timeout )
{
    const char *nativeTimeout = (*env)->GetStringUTFChars(env, timeout, 0);

// --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 10s
    //char *argv[] = {"program name", "arg1", "arg2", NULL};


    enum { kMaxArgs = 64 };
    int argc = 0;
    char *argv[kMaxArgs];
    char *p2 = strtok(nativeTimeout, " ");
    while (p2 && argc < kMaxArgs-1)
    {
        argv[argc++] = p2;
        p2 = strtok(0, " ");
    }
    argv[argc] = 0;



    //char *argv[] = {"--version", "--cpu", "4", "--io", "4", "--vm", "2", "--vm-bytes", "128M", "--timeout", nativeTimeout, NULL};
    //int argc = sizeof(argv) / sizeof(char*) - 1;

    int x = cmd_main (argc, argv);
    char buffer [128];
    int ret = snprintf(buffer, sizeof(buffer), "%ld", x);
    char * num_string = buffer; //String terminator is added by snprintf

    (*env)->ReleaseStringUTFChars(env, timeout, nativeTimeout);
    return (*env)->NewStringUTF(env, num_string);
}
